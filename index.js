//import express JS
const express = require ("express");
//import thư viện path
const path = require ("path")

//khởi tạo app
const app =  express();

//khai báo cổng chạy
const port = 8000;

//call API
app.get("/", (request,response)=>{
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/views/example-call.html"))
})


// chạy app trên cổng đã khai báo
app.listen(port, () =>{
    console.log(`App listening on port ${port}`)
})